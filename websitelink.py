#!/usr/bin/python3

import requests
import simplejson as json
import logging
import sys
import smtplib
import dns.resolver
import traceback
import time

logging.basicConfig(filename="websitelink.log", level=logging.DEBUG)
logging.debug('\n\n***Starting script websitelink.py!')

INFLUX_IP = "172.19.0.51"
INFLUX_PORT = "8086"
INFLUX_USER = "cstat"
INFLUX_PASSWORD = ""

DEBUG = 0

def influxLog(meas, tags, values):
    q = meas
    if tags:
        q += ',' + tags
    q += ' ' + values

    if DEBUG:
        print("Would have sent to db: " + q)
        return

    r = requests.post('http://'+INFLUX_IP+':'+INFLUX_PORT+'/write?db=cstat', data=q.encode(), auth=(INFLUX_USER, INFLUX_PASSWORD))
    if not ((r.status_code >= 200) and (r.status_code < 300)):
        print("Received non 200 on database post: " + str(r.status_code))
        print("\t"+r.text)
        print("\tRequest was: " + str(q))
    else:
        print('Logged with stat code: ' + str(r.status_code))

def caughterror(msg_dbg):
	influxLog('es10-personeelplanner', 'type=error', 'message="'+msg_dbg+'"')

	logging.debug("Sending helpmail to planner@philipgroet.nl")

	answers = dns.resolver.query('philipgroet.nl', 'MX')
	if len(answers) <= 0:
	    logging.error('No mail servers found for destination\n')
	    sys.exit(1)
	 
	# Just pick the first answer
	server = str(answers[0].exchange)
	 
	# Add the From: and To: headers
	fromaddr = 'es10link'
	toaddr = 'planner@philipgroet.nl'
	subject = 'ES10 planner python error (es10side)!'
	body = 'ES10 planner python error!\r\ndebug_msg = ' + msg_dbg
	msg = "From: {}\r\nTo: {}\r\nSubject: {}\r\n\r\n{}".format(fromaddr, toaddr, subject, body)
	 
	server = smtplib.SMTP(server)
	server.set_debuglevel(1)
	server.sendmail(fromaddr, toaddr, msg)
	server.quit()

try:
	config = json.loads(open("config.json", 'r+').read())
	if DEBUG:
		print(config)
except:
	m = "Couldnt open config\n" + traceback.format_exc()
	logging.error(m)
	caughterror(m)


# Upload PLANE.CSV
try:
	start = time.time()

	logging.debug("Uploading plane")
	url = config["urlupload"] + "?token=" + config["token"]

	payload = { "submit":"", "file_name":"PLANE"}

	fd = {"file": open(config["filelocation"] + "PLANE.CSV")}
	r = requests.post(url, files=fd, auth=("philip", "lolservermaster"), data=payload)

	print(r.text)

	if r.status_code != 200:
		logging.debug("Error code not 200")
		raise r.status_code

	influxLog('es10-personeelplanner', 'type=upload,file=PLANE', 'value='+str(time.time() - start))
except:
	m = "FATAL ON PLANE:\n" + traceback.format_exc()
	logging.error(m)
	caughterror(m)


# Upload PLANX.CSV
try:
	start = time.time()

	logging.debug("Uploading planx")
	url = config["urlupload"] + "?token=" + config["token"]

	payload = { "submit":"", "file_name":"PLANX"}

	fd = {"file": open(config["filelocation"] + "PLANX.CSV")}
	r = requests.post(url, files=fd, auth=("philip", "lolservermaster"), data=payload)
	if r.status_code != 200:
		logging.debug("Error code not 200")
		raise r.status_code

	influxLog('es10-personeelplanner', 'type=upload,file=PLANX', 'value='+str(time.time() - start))
except:
	m = "FATAL ON PLANX:\n" + traceback.format_exc()
	logging.error(m)
	caughterror(m)


# Download
try:
	start = time.time()

	logging.debug("Downloading plan_bevestigd.csv")
	url = config["urldownload"] + "?token=" + config["token"]

	download = requests.get(url, auth=("philip", "lolservermaster"))

	f_obj = open(config["filelocation"] + "PLANCON.CSV", 'w+')
	f_obj.write(download.text)
	f_obj.close()
	if download.status_code != 200:
		logging.debug("Error code not 200")
		raise r.status_code

	influxLog('es10-personeelplanner', 'type=download,file=PLANCON', 'value='+str(time.time() - start))
except:
	m = "FATAL ON DOWNLOADING\n" + traceback.format_exc()
	logging.error(m)
	caughterror(m)

	
influxLog('es10-personeelplanner', 'type=heartbeat', 'success=1')
logging.debug('End of script')